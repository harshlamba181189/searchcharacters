import 'core-js';
import 'regenerator-runtime/runtime';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import './assets/sass/index.scss';
import App from './app/App';
import { ServiceFactory } from './app/services/ServiceFactory';
import ServiceContext from './app/services/ServiceContext';

const serviceFactory = new ServiceFactory();

ReactDOM.render(
    <ServiceContext.Provider value={serviceFactory}>
        <App />
    </ServiceContext.Provider>,
    document.getElementById('app')
);
