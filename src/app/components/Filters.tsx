import * as React from 'react';

export default class Filters extends React.Component<{}> {
    public render() {
        return (
            <div className="filters">
                <div className="row justify-content-start">
                    <div className="col-10">
                        <h5 className="filters-heading">Filters</h5>
                    </div>
                    <div className="col-2 hide">
                        <i className="material-icons">keyboard_arrow_down</i>
                    </div>
                </div>
            </div>
        );
    }
}
