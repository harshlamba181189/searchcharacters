import * as React from 'react';
import { ICharacter } from '../App.i';

export interface IProps {
    items: ICharacter[];
}

export default function Characters(props: IProps) {
    const { items } = props;
    const characters: any = [];

    items.forEach(element => {
        const imageStyle = {
            background: `url(${element.image}) no-repeat`,
            backgroundSize: 'cover',
            backgroundPosition: 'center',
        };
        characters.push(
            <div className="col-lg-3 col-md-6 col-12 mb-4" key={element.id}>
                <div className="character-container">
                    <div className="character">
                        <div className="character-profile" style={imageStyle}>
                            <div className="character-profile-info">
                                <div className="character-title">
                                    {element.name} (id: {element.id})
                                </div>
                            </div>
                        </div>
                        <div className="character-details">
                            <div className="character-details-info mb-2">
                                <label className="character-details-label">
                                    STATUS
                                </label>
                                <span className="character-details-value">
                                    {element.status}
                                </span>
                            </div>
                            <div className="character-details-info mb-2">
                                <label className="character-details-label">
                                    SPECIES
                                </label>
                                <span className="character-details-value">
                                    {element.species}
                                </span>
                            </div>
                            <div className="character-details-info mb-2">
                                <label className="character-details-label">
                                    GENDER
                                </label>
                                <span className="character-details-value">
                                    {element.gender}
                                </span>
                            </div>
                            <div className="character-details-info mb-2">
                                <label className="character-details-label">
                                    ORIGIN
                                </label>
                                <span className="character-details-value"></span>
                            </div>
                            <div className="character-details-info character-details-info--no-bottom">
                                <label className="character-details-label">
                                    LAST LOCATION
                                </label>
                                <span className="character-details-value"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    });

    return <div className="row">{characters}</div>;
}
