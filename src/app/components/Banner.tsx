import * as React from 'react';

export default function Banner() {
    return (
        <div className="banner">
            <h2>Please view on Mobile Screen.</h2>
            <h5>Not compatible for Desktop yet</h5>
        </div>
    );
}
