import * as React from 'react';
import ServiceContext from '../services/ServiceContext';
import { ServiceFactory } from '../services/ServiceFactory';
import { PaginationService } from '../services/PaginationService';

export default class Pagination extends React.Component<{}> {
    static contextType = ServiceContext;
    private _paginationService: PaginationService;
    constructor(props: {}) {
        super(props);

        this.state = {
            isNextDisabled: true,
            isPrevDisabled: true,
        };
    }

    public componentDidMount() {
        const serviceFactory: ServiceFactory = this.context;
        this._paginationService = serviceFactory.getPaginationService();

        this._paginationService.dataRefereshed.subscribe(
            this.onDataRefereshed.bind(this)
        );
    }

    public render() {
        return (
            <React.Fragment>
                <nav aria-label="Page navigation">
                    <ul className="pagination pagination-sm justify-content-center">
                        <li className="page-item">
                            <a
                                className="page-link"
                                href="#"
                                onClick={this.prev.bind(this)}
                                aria-label="Previous"
                            >
                                <span aria-hidden="true">&laquo;</span>
                                <span className="sr-only">Previous</span>
                            </a>
                        </li>
                        <li className="page-item">
                            <a
                                className="page-link"
                                href="#"
                                aria-label="Next"
                                onClick={this.next.bind(this)}
                            >
                                <span aria-hidden="true">&raquo;</span>
                                <span className="sr-only">Next</span>
                            </a>
                        </li>
                    </ul>
                </nav>
            </React.Fragment>
        );
    }

    private onDataRefereshed() {
        this.setState({
            isNextDisabled: this._paginationService.isNextDisabled,
            isPrevDisabled: this._paginationService.isPrevDisabled,
        });
    }

    private next() {
        this._paginationService.next();
    }

    private prev() {
        this._paginationService.prev();
    }

    public componentWillUnmount() {
        this._paginationService &&
            this._paginationService.dataRefereshed.unSubscribe(
                this.onDataRefereshed.bind(this)
            );
    }
}
