import * as React from 'react';

interface ILoaderProps {
    isShow: boolean;
}

export default function Loader(props: ILoaderProps) {
    return (
        props.isShow && (
            <div className="loader-overlay">
                <div className="loader">Loading...</div>
            </div>
        )
    );
}
