import { IEvent } from './core/Event';

export interface ICharacter {
    id: string;
    name: string;
    status: string;
    species: string;
    gender: string;
    image: string;
    created: string;
}

export interface IInfo {
    pages: number;
    next: string;
    prev: string;
}

export interface IDataProviderService {
    info: IInfo;
    characters: ICharacter[];
    dataFetchStart: IEvent;
    dataRefereshed: IEvent;
    fetchData(url: string): Promise<void>;
}

export interface IPaginationService {
    pagesCount: number;
    isNextDisabled: boolean;
    isPrevDisabled: boolean;
    dataRefereshed: IEvent;
    next(): void;
    prev(): void;
}

export class Character implements ICharacter {
    private _id: string;
    private _name: string;
    private _status: string;
    private _species: string;
    private _gender: string;
    private _image: string;
    private _created: string;
    public get id(): string {
        return this._id;
    }

    public get name(): string {
        return this._name;
    }

    public get status(): string {
        return this._status;
    }

    public get species(): string {
        return this._species;
    }

    public get gender(): string {
        return this._gender;
    }

    public get image(): string {
        return this._image;
    }

    public get created(): string {
        return this._created;
    }

    public referesh(rawData: { [key: string]: any }): void {
        this._id = rawData['id'];
        this._name = rawData['name'];
        this._status = rawData['status'];
        this._species = rawData['species'];
        this._gender = rawData['gender'];
        this._image = rawData['image'];
        this._created = rawData['created'];
    }
}
