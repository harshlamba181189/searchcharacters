import * as React from 'react';
import ServiceContext from './services/ServiceContext';
import { DataProviderService } from './services/DataProviderService';
import { AppConstants } from './AppConstants';
import { ICharacter } from './App.i';

import Pagination from './components/Pagination';
import Header from './components/Header';
import Filters from './components/Filters';
import Characters from './components/Characters';
import Loader from './components/Loader';
import Banner from './components/Banner';

interface IAppState {
    characters: ICharacter[];
    isLoader: boolean;
    sortOrder: string;
}

export default class App extends React.Component<{}, IAppState> {
    static contextType = ServiceContext;
    private _dataProviderService: DataProviderService;

    constructor(props: {}) {
        super(props);

        this.state = {
            characters: [],
            isLoader: false,
            sortOrder: 'ASCN',
        };
    }

    public render() {
        return (
            <React.Fragment>
                <Banner />
                <Loader isShow={this.state.isLoader} />
                <Header />
                <div className="container">
                    <div className="row">
                        <div className="col-md-2 col-12 mb-2">
                            <Filters />
                        </div>
                        <div className="col-md-10 col-12">
                            <div className="row">
                                <div className="col-md-6 col-12">
                                    <h5>Selected Filters</h5>
                                </div>
                                <div className="col-md-6 col-12">
                                    <div className="row">
                                        <div className="col-12">
                                            <div className="form-group">
                                                <label>Sort By(Id):</label>
                                                <select
                                                    id="sortBy"
                                                    className="form-control form-control-sm"
                                                    value={this.state.sortOrder}
                                                    onChange={this.onSortSelectionChanged.bind(
                                                        this
                                                    )}
                                                >
                                                    <option value="ASCN">
                                                        Ascending
                                                    </option>
                                                    <option value="DES">
                                                        Descending
                                                    </option>
                                                </select>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <Pagination />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <Characters items={this.state.characters} />
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }

    public componentDidMount() {
        const serviceFactory = this.context;
        this._dataProviderService = serviceFactory.getDataProviderService();
        this._dataProviderService.fetchData(AppConstants.API_URL);

        this._dataProviderService.dataRefereshed.subscribe(
            this.onDataRefereshed.bind(this)
        );
        this._dataProviderService.dataFetchStart.subscribe(
            this.onDataFetchStart.bind(this)
        );
    }

    private onDataRefereshed() {
        this.setState({
            characters: this._dataProviderService.characters,
            isLoader: false,
        });
    }

    private onDataFetchStart() {
        this.setState({
            characters: this._dataProviderService.characters,
            isLoader: true,
            sortOrder: 'ASCN',
        });
    }

    private onSortSelectionChanged(event: any) {
        let sortedCharacters: ICharacter[] = [];
        const order = event.target.value;

        switch (order) {
            case 'ASCN':
                sortedCharacters = this._dataProviderService.characters.sort(
                    this.ascendingSort
                );
                break;
            case 'DES':
                sortedCharacters = this._dataProviderService.characters.sort(
                    this.descendingSort
                );
                break;
        }

        this.setState({
            sortOrder: event.target.value,
            characters: sortedCharacters,
            isLoader: false,
        });
    }

    private ascendingSort(a: ICharacter, b: ICharacter) {
        return a.id > b.id ? 1 : b.id > a.id ? -1 : 0;
    }

    private descendingSort(a: ICharacter, b: ICharacter) {
        return a.id > b.id ? -1 : b.id > a.id ? 1 : 0;
    }

    public componentWillUnmount() {
        this._dataProviderService &&
            this._dataProviderService.dataRefereshed.unSubscribe(
                this.onDataRefereshed
            );
    }
}
