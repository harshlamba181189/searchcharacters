import { IDataProviderService, IInfo, ICharacter, Character } from '../App.i';
import Event, { IEvent } from '../core/Event';
import Logger from '../core/Logger';

export class DataProviderService implements IDataProviderService {
    private _dataRefereshed: Event;
    private _info: IInfo;
    private _characters: ICharacter[];
    private _dataFetchStart: Event;

    constructor() {
        this._dataRefereshed = new Event();
        this._dataFetchStart = new Event();
        this._info = <IInfo>{};
        this._characters = [];
    }

    public get info(): IInfo {
        return this._info;
    }

    public get characters(): ICharacter[] {
        return this._characters;
    }

    public get dataFetchStart(): IEvent {
        return this._dataFetchStart;
    }

    public get dataRefereshed(): IEvent {
        return this._dataRefereshed;
    }

    public async fetchData(url: string): Promise<void> {
        if (!url) Logger.error('Url cannot be empty');

        this._dataFetchStart.raise();

        const response = await fetch(url);
        const data = await response.json();

        this._info = data['info'];
        this._characters =
            data['results'] &&
            data['results'].map((item: any) => {
                const character = new Character();
                character.referesh(item);

                return character;
            });

        this._dataRefereshed.raise();
        return data;
    }
}
