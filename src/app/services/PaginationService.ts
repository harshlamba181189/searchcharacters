import { IPaginationService } from '../App.i';
import { ServiceFactory } from './ServiceFactory';
import { DataProviderService } from './DataProviderService';
import Event, { IEvent } from '../core/Event';

export class PaginationService implements IPaginationService {
    private _isNextDisabled: boolean;
    private _isPrevDisabled: boolean;
    private _pagesCount: number;
    private _dataProviderService: DataProviderService;
    private _dataRefereshed: Event;
    constructor(serviceFactory: ServiceFactory) {
        this._dataProviderService = serviceFactory.getDataProviderService();
        this._isNextDisabled = true;
        this._isPrevDisabled = true;

        this._dataRefereshed = new Event();

        this._dataProviderService.dataRefereshed.subscribe(
            this.onDataRefereshed.bind(this)
        );
    }

    public get dataRefereshed(): IEvent {
        return this._dataRefereshed;
    }

    public get pagesCount(): number {
        return this._pagesCount;
    }

    public get isNextDisabled(): boolean {
        return this._isNextDisabled;
    }

    public get isPrevDisabled(): boolean {
        return this._isPrevDisabled;
    }

    public next(): void {
        const nextUrl = this._dataProviderService.info.next;
        this._dataProviderService.fetchData(nextUrl);
    }

    public prev(): void {
        const prevUrl = this._dataProviderService.info.prev;
        this._dataProviderService.fetchData(prevUrl);
    }

    private onDataRefereshed() {
        this._isNextDisabled = !!this._dataProviderService.info.next;
        this._isPrevDisabled = !!this._dataProviderService.info.prev;

        this._dataRefereshed.raise();
    }

    public dispose() {}
}
