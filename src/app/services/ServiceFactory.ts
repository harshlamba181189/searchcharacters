import { DataProviderService } from './DataProviderService';
import { PaginationService } from './PaginationService';

export class ServiceFactory {
    private _dataProviderService: DataProviderService;
    private _paginationService: PaginationService;

    public getDataProviderService(): DataProviderService {
        if (!this._dataProviderService)
            this._dataProviderService = new DataProviderService();

        return this._dataProviderService;
    }

    public getPaginationService(): PaginationService {
        if (!this._paginationService)
            this._paginationService = new PaginationService(this);

        return this._paginationService;
    }
}
